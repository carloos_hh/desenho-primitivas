
import javax.swing.JFrame;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.time.Clock.system;
import static java.util.Collections.min;

public class Linhas implements GLEventListener {

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        gl.glColor3f(1f, 1f, 1f);
        gl.glEnable(GL2.GL_LINE_STIPPLE);
        gl.glLineStipple(1, (short) 0xF0F0);
        
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2i(0, 0);
        gl.glVertex2i(200, 200);
        gl.glEnd();
        gl.glDisable(GL2.GL_LINE_STIPPLE);
        
        gl.glLineWidth(5f);
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2i(15, 0);
        gl.glVertex2i(215, 200);
        gl.glEnd();
        
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2i(5,0 );
        gl.glVertex2i(205, 200);
        gl.glEnd();
        
        gl.glLineWidth(5f);
        gl.glEnable(GL2.GL_LINE_STIPPLE);
        gl.glLineStipple(1, (short) 0xAAAA);
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2i(25,0 );
        gl.glVertex2i(225, 200);
        gl.glEnd();
        gl.glDisable(GL2.GL_LINE_STIPPLE);

        
        

        gl.glFlush();

    }


    @Override
    public void dispose(GLAutoDrawable arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glMatrixMode(GL2.GL_MATRIX_MODE);
        gl.glLoadIdentity();

        GLU glu = new GLU();
        glu.gluOrtho2D(0, 200f, 0f, 150f);

    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {

        GLProfile profile = GLProfile.get(GLProfile.GL2);

        GLCapabilities caps = new GLCapabilities(profile);

        GLCanvas canvas = new GLCanvas(caps);
        canvas.addGLEventListener(new Linhas());

        JFrame frame = new JFrame("Primeiro Programa OPENGL");

        frame.add(canvas);
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
